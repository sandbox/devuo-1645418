
-- SUMMARY --

Select2 module adds integration with the Select2 Javascript plugin.


-- INSTALLATION --

* Download the latest version of Select2, extract it's contents to
  'sites/all/libraries' folder, and re-name the extracted folder to 'select2', so
  as the path to be 'sites/all/libraries/select2/select2.js'.


-- REQUIREMENTS --

* Libraries module 7.x-2.0 (higher or equal)


-- CONTACT --

Current maintainers:
* Diogo Correia (devuo) - http://drupal.org/user/887060
* Filipe Guerra (alias.mac) - http://drupal.org/user/736918


This project has been sponsored by:
* DRI - Open Solutions for Open Minds
  www.dri-global.com
